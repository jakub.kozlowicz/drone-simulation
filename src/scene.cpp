#include "scene.hh"

#include <chrono>
#include <thread>

using namespace std;
using namespace std::this_thread;
using namespace std::chrono;

Scene::Scene() {
    link_.Init();
    link_.SetDrawingMode(PzG::DM_3D);

    bottom_surface_.draw(bottom_surface_.kBottomSurfaceFile_);
    link_.AddFilename(bottom_surface_.kBottomSurfaceFile_.c_str(), PzG::LS_CONTINUOUS, 1);

    water_surface_.draw(water_surface_.kWaterSurfaceFile_);
    link_.AddFilename(water_surface_.kWaterSurfaceFile_.c_str(), PzG::LS_CONTINUOUS, 1);

    rod_.draw(rod_.kObstacleRodFile);
    link_.AddFilename(rod_.kObstacleRodFile.c_str(), PzG::LS_CONTINUOUS, 1);

    rectangle_.draw(rectangle_.kObstacleRectangleFile);
    link_.AddFilename(rectangle_.kObstacleRectangleFile.c_str(), PzG::LS_CONTINUOUS, 1);

    cuboid_obstacle_.draw(cuboid_obstacle_.kObstacleCuboidFile);
    link_.AddFilename(cuboid_obstacle_.kObstacleCuboidFile.c_str(), PzG::LS_CONTINUOUS, 1);

    drone_.draw(drone_.kDroneFile,
                drone_.kLeftFrontRotorFile_,
                drone_.kRightFrontRotorFile_,
                drone_.kLeftRearRotorFile_,
                drone_.kRightRearRotorFile_);
    link_.AddFilename(drone_.kDroneFile.c_str(), PzG::LS_CONTINUOUS, 1);
    link_.AddFilename(drone_.kLeftFrontRotorFile_.c_str(), PzG::LS_CONTINUOUS, 1);
    link_.AddFilename(drone_.kRightFrontRotorFile_.c_str(), PzG::LS_CONTINUOUS, 1);
    link_.AddFilename(drone_.kLeftRearRotorFile_.c_str(), PzG::LS_CONTINUOUS, 1);
    link_.AddFilename(drone_.kRightRearRotorFile_.c_str(), PzG::LS_CONTINUOUS, 1);

    link_.Draw();
}

bool Scene::checkCollision(const vector<Vector3D> &droneCover) {
    vector<shared_ptr<Shape>>
        objects = objectsOnScene<Drone, bottomSurface, waterSurface, Rod, Rectangle, CuboidObstacle>();

    for (const auto &object : objects) {
        if (object->checkCollision(droneCover)) {
            object->getCollisionInformation();
            return true;
        }
    }

    return false;
}

void Scene::animation(double rotateAngle) {

    for (unsigned i = 0; i < frames_; ++i) {
        drone_.rotate(rotateAngle, frames_);
        link_.Draw();
        sleep_for(microseconds(frameFreeze_));
    }
}

void Scene::animation(const double &angleChange, const double &distance) {

    for (unsigned i = 0; i < frames_; ++i) {
        if (checkCollision(drone_.moveForward(angleChange, distance, frames_))) {
            break;
        }
        link_.Draw();
        sleep_for(microseconds(frameFreeze_));

    }
}

void Scene::displayObjectCount() {
    cout << "   Current number of Vector3D objects: " << Shape::currentCounter_ << endl;
    cout << " The total number of Vector3D objects: " << Shape::allCounter_ << endl;
    cout << endl;
}
