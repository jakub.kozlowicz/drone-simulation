#include "cuboid.hh"

using namespace std;

Cuboid::Cuboid() {
    // Open file with model coordinates of cuboid
    ifstream inputFile;
    inputFile.open(kModelCuboid_);

    // Check if the file is open correctly
    if (!inputFile.is_open()) {
        cerr << "Unable to load model Cuboid file!" << endl;
        return;
    }

    // Read all of model coordinates
    Vector3D point;
    while (inputFile >> point) {
        points_.push_back(point);
        allCounter_++; // Increase counter of total number of Vector3D objects
        currentCounter_++; // Increase counter of current number of Vector3D objects

        // Read the enlarged drone body as a drone cover later used for collision checking
        cuboidCover_.push_back(point * 1.5);
    }
    inputFile.close();
}

void Cuboid::draw(const std::string &filename) const {
    // Open file to which all coordinates will be saved
    ofstream outputFile;
    outputFile.open(filename);

    // Check if the file is open correctly
    if (!outputFile.is_open()) {
        cerr << "Unable to open drone file!" << endl;
        return;
    }

    // Rotation matrix to rotate the drone in OZ axis
    RotationMatrix matrix("OZ", angle_);

    // Save all coordinates to the result file
    for (unsigned i = 0; i < points_.size(); ++i) {
        outputFile << (matrix * points_[i]) + translation_ << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

void Cuboid::translateCover(const Vector3D &change) {
    // Translate all of drone cover coordinate with the same translation vector as drone body
    for (auto &i : cuboidCover_) {
        i = i + change;
    }

}

vector<Vector3D> Cuboid::getNextStepCover(const Vector3D &translation) const {
    vector<Vector3D> nextStepCover;

    // Reserve memory for drone cover in nest step of movement
    nextStepCover.reserve(cuboidCover_.size());

    for (const auto &i : cuboidCover_) {
        nextStepCover.push_back(i + translation);
    }

    return nextStepCover;
}
