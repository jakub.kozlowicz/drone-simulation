#include "bottomSurface.hh"

using namespace std;

bottomSurface::bottomSurface() {
    // Open file with model coordinates of bottom surface
    ifstream inputFile;
    inputFile.open(kBottomModel_);

    // Check if the file is open correctly
    if (!inputFile.is_open()) {
        cerr << "Unable to load bottom model file!" << endl;
        return;
    }

    // Read all of model coordinates
    Vector3D point;
    while (inputFile >> point) {
        bottom_.push_back(point);
        allCounter_++; // Increase counter of total number of Vector3D objects
        currentCounter_++; // Increase counter of current number of Vector3D objects
    }
    inputFile.close();
}

void bottomSurface::draw(const string &filename) const {
    // Open file to which all coordinates will be saved
    ofstream outputFile;
    outputFile.open(filename);

    // Check if the file is open correctly
    if (!outputFile.is_open()) {
        cerr << "Unable to open bottom file!" << endl;
        return;
    }

    // Save all coordinates to the result file
    for (unsigned i = 0; i < bottom_.size(); ++i) {
        outputFile << bottom_[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

bool bottomSurface::checkCollision(const vector<Vector3D> &droneCover) const {
    // Checks if any of the Z coordinates is less than the bottom level
    for (const auto &position : droneCover) {
        if (position[2] < bottomLevel_) {
            return true;
        }
    }

    return false;
}
