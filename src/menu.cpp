#include "menu.hh"

#include <iostream>

using namespace std;

void menu() {
    cout << endl;
    cout << " r - make a forward move" << endl;
    cout << " o - rotate the drone" << endl;
    cout << " m - display menu" << endl;
    cout << endl;
    cout << " k - end of program" << endl;
    cout << endl;
}