#include "waterSurface.hh"

using namespace std;

waterSurface::waterSurface() {
    // Open file with model coordinates of water surface
    ifstream inputFile;
    inputFile.open(kWaterModel_);

    // Check if the file is open correctly
    if (!inputFile.is_open()) {
        cerr << "Unable to load water model file!" << endl;
        return;
    }

    // Read all of model coordinates
    Vector3D point;
    while (inputFile >> point) {
        water_.push_back(point);
        allCounter_++; // Increase counter of total number of Vector3D objects
        currentCounter_++; // Increase counter of current number of Vector3D objects
    }
    inputFile.close();
}

void waterSurface::draw(const string &filename) const {
    // Open file to which all coordinates will be saved
    ofstream outputFile;
    outputFile.open(filename);

    // Check if the file is open correctly
    if (!outputFile.is_open()) {
        cerr << "Unable to open water file!" << endl;
        return;
    }

    // Save all coordinates to the result file
    for (unsigned i = 0; i < water_.size(); ++i) {
        outputFile << water_[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

bool waterSurface::checkCollision(const vector<Vector3D> &droneCover) const {
// Maximal and minimal values of coordinates of drone cover initialized with NaN values,
// to assign the appropriate values to them at the first pass of the data download loop.
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    // Find minimal and maximal value of Z axis coordinates
    for (int i = 1; i < droneCover.size(); ++i) {
        if (!(droneCover[i][2] >= coverZMin)) coverZMin = droneCover[i][2]; // Calculate minimal value on Z axis
        if (!(droneCover[i][2] <= coverZMax)) coverZMax = droneCover[i][2]; // Calculate maximal value on Z axis
    }

    double centerDrone = 0.5 * (coverZMin + coverZMax);

    return centerDrone > waterLevel_;

}
