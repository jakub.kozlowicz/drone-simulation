#include <iostream>
#include <climits>

#include "menu.hh"
#include "scene.hh"

using namespace std;

int main() {
    cout << endl;
    cout << " Program start " << endl;
    cout << endl;

    char character = '-';
    Scene scene;

    menu();
    while (character != 'k') {
        cout << endl;

        Scene::displayObjectCount();

        cout << " Your choice, m - menu > ";
        cin >> character;

        if (character != 'k' && character != 'r' && character != 'o' && character != 'm') {
            cerr << endl << " Your choice was incorrect " << endl << endl;
            cin.clear();
            cin.ignore(INT_MAX, '\n');
        }

        switch (character) {
            case 'r': {  // Move forward
                double angle = 0;
                double distance = 0;
                cout << "Enter the angle (climb / fall) in degrees." << endl;
                cout << "Angle value > ";
                while (!(cin >> angle)) {
                    cin.clear();
                    cin.ignore(INT_MAX, '\n');
                    cerr << endl << " Your passed incorrect value " << endl << endl;
                    cout << "Angle value > ";
                }

                cout << "Enter the distance to which the drone should move." << endl;
                cout << "Distance value > ";
                while (!(cin >> distance)) {
                    cin.clear();
                    cin.ignore(INT_MAX, '\n');
                    cerr << endl << " Your passed incorrect value " << endl << endl;
                    cout << "Distance value > ";
                }

                scene.animation(angle, distance);

                break;
            }
            case 'o': {  // Change orientation (Oz axis)
                double angle = 0;
                cout << "Enter the angle in degrees you want to rotate the drone > ";
                while (!(cin >> angle)) {
                    cin.clear();
                    cin.ignore(INT_MAX, '\n');
                    cerr << endl << " Your passed incorrect value " << endl << endl;
                    cout << "Enter the angle in degrees you want to rotate the drone > ";
                }

                scene.animation(angle);

                break;
            }
            case 'm': {  // Display menu
                menu();
                break;
            }
            default: break;
        }
    }

    cout << endl;
    cout << " End of program " << endl;
    cout << endl;
}
