#include "shape.hh"

Shape::minMax Shape::getMinMaxValues(const std::vector<Vector3D> &object) const {
    minMax result{};

    // Set minimal and maximal values to appropriate axis
    result.xMin = std::numeric_limits<double>::max(), result.xMax = std::numeric_limits<double>::min();  // X axis
    result.yMin = std::numeric_limits<double>::max(), result.yMax = std::numeric_limits<double>::min();  // Y axis
    result.zMin = std::numeric_limits<double>::max(), result.zMax = std::numeric_limits<double>::min();  // Z axis

    for (int i = 1; i < object.size(); ++i) {
        if (object[i][0] < result.xMin) result.xMin = object[i][0];  // Calculate minimal value on X axis
        if (object[i][0] > result.xMax) result.xMax = object[i][0];  // Calculate maximal value on X axis

        if (object[i][1] < result.yMin) result.yMin = object[i][1];  // Calculate minimal value on Y axis
        if (object[i][1] > result.yMax) result.yMax = object[i][1];  // Calculate maximal value on Y axis

        if (object[i][2] < result.zMin) result.zMin = object[i][2];  // Calculate minimal value on Z axis
        if (object[i][2] > result.zMax) result.zMax = object[i][2];  // Calculate maximal value on Z axis
    }

    return result;
}
