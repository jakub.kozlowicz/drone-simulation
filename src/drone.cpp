#include <memory>
#include "drone.hh"

using namespace std;

vector<Vector3D> Drone::moveForward(const double &angleChange, const double &distance, const int &frames) {
    //Calculate translation in each frames of animation
    Vector3D translation = (calculateTranslation(angleChange, distance) / frames);

    Cuboid::translate(translation); // Translate drone body
    Cuboid::translateCover(translation); // Translate drone cover
    rotateRotor(frames); // Rotate rotors

    // Save all coordinates to results files
    draw(kDroneFile, kLeftFrontRotorFile_, kRightFrontRotorFile_, kLeftRearRotorFile_, kRightRearRotorFile_);

    return Cuboid::getNextStepCover(translation); // Drone cover in next step
}

void Drone::rotate(const double &rotateAngle, const int &frames) {

    Cuboid::writeAngle(rotateAngle / frames); // Rotate the drone
    rotateRotor(frames); // Rotate the rotors

    // Save all coordinates to results files
    draw(kDroneFile, kLeftFrontRotorFile_, kRightFrontRotorFile_, kLeftRearRotorFile_, kRightRearRotorFile_);
}

Vector3D Drone::calculateTranslation(double angleChange, double distance) const {
    Vector3D result;

    result[0] = (cos(makeRadians(angleChange)) * distance) * cos(makeRadians(angle_)); // OX axis
    result[1] = (cos(makeRadians(angleChange)) * distance) * sin(makeRadians(angle_)); // OY axis
    result[2] = (sin(makeRadians(angleChange)) * distance); // OZ axis

    return result;
}
bool Drone::checkCollision(const std::vector<Vector3D> &droneCover) const {
    return false;
}

Drone::Drone() {
    // Open file with model coordinates of drone
    ifstream inputFile;
    inputFile.open(kRotorModel_);

    // Check if the file is open correctly
    if (!inputFile.is_open()) {
        cerr << "Unable to load model rotor file!" << endl;
        return;
    }

    // Read all of model coordinates
    Vector3D point;
    while (inputFile >> point) {
        leftFrontRotor_.push_back(point);
        rightFrontRotor_.push_back(point);
        leftRearRotor_.push_back(point);
        rightRearRotor_.push_back(point);

        currentCounter_ += 4; // Increase counter of total number of Vector3D objects
        allCounter_ += 4; // Increase counter of current number of Vector3D objects
    }
    inputFile.close();
}

void Drone::draw(const std::string &cuboidFilename,
                 const std::string &leftFrontRotorFilename,
                 const std::string &rightFrontRotorFilename,
                 const std::string &leftRearRotorFilename,
                 const std::string &rightRearRotorFilename) const {
    // Save drone body ro result file
    Cuboid::draw(cuboidFilename);

    // Rotation matrix in OZ axis
    RotationMatrix matrix("OZ", angle_);

    // Open files to which all rotors coordinates will be saved
    ofstream outputFile, outputFile2, outputFile3, outputFile4;
    outputFile.open(leftFrontRotorFilename);
    outputFile2.open(rightFrontRotorFilename);
    outputFile3.open(leftRearRotorFilename);
    outputFile4.open(rightRearRotorFilename);

    // Check if the files are open correctly
    if (!outputFile.is_open() || !outputFile2.is_open() || !outputFile3.is_open() || !outputFile4.is_open()) {
        cerr << "Unable to open rotors files!" << endl;
        return;
    }

    // Save all coordinates to the result file
    for (unsigned i = 0; (i < leftFrontRotor_.size() || i < rightFrontRotor_.size() || i < leftRearRotor_.size()
        || i < rightRearRotor_.size()); ++i) {
        outputFile << (matrix * (leftFrontRotor_[i] + leftFrontRotorTranslation_)) + translation_ << endl;
        outputFile2 << (matrix * (rightFrontRotor_[i] + rightFrontRotorTranslation_)) + translation_ << endl;
        outputFile3 << (matrix * (leftRearRotor_[i] + leftRearRotorTranslation_)) + translation_ << endl;
        outputFile4 << (matrix * (rightRearRotor_[i] + rightRearRotorTranslation_)) + translation_ << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
            outputFile2 << "#\n\n";
            outputFile3 << "#\n\n";
            outputFile4 << "#\n\n";
        }
    }

    outputFile.close();
    outputFile2.close();
    outputFile3.close();
    outputFile4.close();
}

void Drone::rotateRotor(const int &frames) {
    // Rotation matrix in OX axis
    RotationMatrix rotate("OY",(360.0 / frames));

    for (auto &i : leftFrontRotor_) i = rotate * i;

    for (auto &i : rightFrontRotor_) i = rotate * i;

    for (auto &i : leftRearRotor_) i = rotate * i;

    for (auto &i : rightRearRotor_) i = rotate * i;
}
