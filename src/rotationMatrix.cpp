#include <cmath>

#include "rotationMatrix.hh"

Vector3D RotationMatrix::operator*(const Vector3D &vector) const {
    Vector3D result;

    result = rotationMatrix_ * vector;

    return result;
}

RotationMatrix::RotationMatrix(const std::string &axis, const double &angle) {
    double degree = angle * PI / 180;

    if (axis[1] == 'X') {
        rotationMatrix_(0, 0) = 1;
        rotationMatrix_(0, 1) = 0;
        rotationMatrix_(0, 2) = 0;
        rotationMatrix_(1, 0) = 0;
        rotationMatrix_(1, 1) = cos(degree);
        rotationMatrix_(1, 2) = -sin(degree);
        rotationMatrix_(2, 0) = 0;
        rotationMatrix_(2, 1) = sin(degree);
        rotationMatrix_(2, 2) = cos(degree);
    }

    if (axis[1] == 'Y') {
        rotationMatrix_(0, 0) = cos(degree);
        rotationMatrix_(0, 1) = 0;
        rotationMatrix_(0, 2) = sin(degree);
        rotationMatrix_(1, 0) = 0;
        rotationMatrix_(1, 1) = 1;
        rotationMatrix_(1, 2) = 0;
        rotationMatrix_(2, 0) = -sin(degree);
        rotationMatrix_(2, 1) = 0;
        rotationMatrix_(2, 2) = cos(degree);
    }

    if (axis[1] == 'Z') {
        rotationMatrix_(0, 0) = cos(degree);
        rotationMatrix_(0, 1) = -sin(degree);
        rotationMatrix_(0, 2) = 0;
        rotationMatrix_(1, 0) = sin(degree);
        rotationMatrix_(1, 1) = cos(degree);
        rotationMatrix_(1, 2) = 0;
        rotationMatrix_(2, 0) = 0;
        rotationMatrix_(2, 1) = 0;
        rotationMatrix_(2, 2) = 1;
    }
}
