#include "obstacles.hh"

using namespace std;

Rod::Rod() {
    // Open file with model coordinates of rod obstacle
    ifstream inputFile;
    inputFile.open(kObstacleRodModel);

    // Check if the file is open correctly
    if (!inputFile.is_open()) {
        cerr << "Unable to load model rod obstacle file!" << endl;
        return;
    }

    // Read all of model coordinates
    Vector3D point;
    while (inputFile >> point) {
        point = point + obstacleRodTranslation_;
        obstacleRod_.push_back(point);
        allCounter_++; // Increase counter of total number of Vector3D objects
        currentCounter_++; // Increase counter of current number of Vector3D objects

        if (!(point[0] >= xMin)) xMin = point[0]; // Calculate minimal value of coordinate on X axis
        if (!(point[0] <= xMax)) xMax = point[0]; // Calculate maximal value of coordinate on X axis

        if (!(point[1] >= yMin)) yMin = point[1]; // Calculate minimal value of coordinate on Y axis
        if (!(point[1] <= yMax)) yMax = point[1]; // Calculate maximal value of coordinate on Y axis

        if (!(point[2] >= zMin)) zMin = point[2]; // Calculate minimal value of coordinate on Z axis
        if (!(point[2] <= zMax)) zMax = point[2]; // Calculate maximal value of coordinate on Z axis
    }
    inputFile.close();
}

void Rod::draw(const string &rodFilename) const {
    // Open file to which all coordinates will be saved
    ofstream outputFile;
    outputFile.open(rodFilename);

    // Check if the file is open correctly
    if (!outputFile.is_open()) {
        cerr << "Unable to open obstacle rod file!" << endl;
        return;
    }

    // Save all coordinates to the result file
    for (unsigned i = 0; i < obstacleRod_.size(); ++i) {
        outputFile << obstacleRod_[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}
bool Rod::checkCollision(const std::vector<Vector3D> &droneCover) const {
    // Maximal and minimal values of coordinates of drone cover initialized with NaN values,
    // to assign the appropriate values to them at the first pass of the data download loop.
    double coverXMin = 0.0 / 0.0, coverXMax = 0.0 / 0.0;
    double coverYMin = 0.0 / 0.0, coverYMax = 0.0 / 0.0;
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    auto result = Shape::getMinMaxValues(droneCover);

    return (result.xMin <= xMax && result.xMax >= xMin) && (result.yMin <= yMax && result.yMax >= yMin)
        && (result.zMin <= zMax && result.zMax >= zMin);
}

Rectangle::Rectangle() {
    // Open file with model coordinates of rectangle obstacle
    ifstream inputFile;
    inputFile.open(kObstacleRectangleModel);

    // Check if the file is open correctly
    if (!inputFile.is_open()) {
        cerr << "Unable to load model obstacle rectangle file!" << endl;
        return;
    }

    // Read all of model coordinates
    Vector3D point;
    while (inputFile >> point) {
        point = point + obstacleRectangleTranslation_;
        obstacleRectangle_.push_back(point);
        allCounter_++; // Increase counter of total number of Vector3D objects
        currentCounter_++; // Increase counter of current number of Vector3D objects

        if (!(point[0] >= xMin)) xMin = point[0]; // Calculate minimal value of coordinate on X axis
        if (!(point[0] <= xMax)) xMax = point[0]; // Calculate maximal value of coordinate on X axis

        if (!(point[1] >= yMin)) yMin = point[1]; // Calculate minimal value of coordinate on Y axis
        if (!(point[1] <= yMax)) yMax = point[1]; // Calculate maximal value of coordinate on Y axis

        if (!(point[2] >= zMin)) zMin = point[2]; // Calculate minimal value of coordinate on Z axis
        if (!(point[2] <= zMax)) zMax = point[2]; // Calculate maximal value of coordinate on Z axis
    }
    inputFile.close();
}

void Rectangle::draw(const string &rectangleFilename) const {
    // Open file to which all coordinates will be saved
    ofstream outputFile;
    outputFile.open(rectangleFilename);

    // Check if the file is open correctly
    if (!outputFile.is_open()) {
        cerr << "Unable to open obstacle rectangle file!" << endl;
        return;
    }

    // Save all coordinates to the result file
    for (unsigned i = 0; i < obstacleRectangle_.size(); ++i) {
        outputFile << obstacleRectangle_[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

bool Rectangle::checkCollision(const std::vector<Vector3D> &droneCover) const {
    // Maximal and minimal values of coordinates of drone cover initialized with NaN values,
    // to assign the appropriate values to them at the first pass of the data download loop.
    double coverXMin = 0.0 / 0.0, coverXMax = 0.0 / 0.0;
    double coverYMin = 0.0 / 0.0, coverYMax = 0.0 / 0.0;
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    auto result = Shape::getMinMaxValues(droneCover);

    return (result.xMin <= xMax && result.xMax >= xMin) && (result.yMin <= yMax && result.yMax >= yMin)
        && (result.zMin <= zMax && result.zMax >= zMin);
}

CuboidObstacle::CuboidObstacle() {
    // Open file with model coordinates of cuboid obstacle
    ifstream inputFile;
    inputFile.open(kObstacleCuboidModel);

    // Check if the file is open correctly
    if (!inputFile.is_open()) {
        cerr << "Unable to load model obstacle cuboid file!" << endl;
        return;
    }

    // Read all of model coordinates
    Vector3D point;
    while (inputFile >> point) {
        point = point + obstacleCuboidTranslation_;
        obstacleCuboid_.push_back(point);
        allCounter_++; // Increase counter of total number of Vector3D objects
        currentCounter_++; // Increase counter of current number of Vector3D objects

        if (!(point[0] >= xMin)) xMin = point[0]; // Calculate minimal value of coordinate on X axis
        if (!(point[0] <= xMax)) xMax = point[0]; // Calculate maximal value of coordinate on X axis

        if (!(point[1] >= yMin)) yMin = point[1]; // Calculate minimal value of coordinate on Y axis
        if (!(point[1] <= yMax)) yMax = point[1]; // Calculate maximal value of coordinate on Y axis

        if (!(point[2] >= zMin)) zMin = point[2]; // Calculate minimal value of coordinate on Z axis
        if (!(point[2] <= zMax)) zMax = point[2]; // Calculate maximal value of coordinate on Z axis
    }
    inputFile.close();
}

void CuboidObstacle::draw(const string &cuboidObstacleFilename) const {
    // Open file to which all coordinates will be saved
    ofstream outputFile;
    outputFile.open(cuboidObstacleFilename);

    // Check if the file is open correctly
    if (!outputFile.is_open()) {
        cerr << "Unable to open obstacle cuboid file!" << endl;
        return;
    }

    // Save all coordinates to the result file
    for (unsigned i = 0; i < obstacleCuboid_.size(); ++i) {
        outputFile << obstacleCuboid_[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

bool CuboidObstacle::checkCollision(const std::vector<Vector3D> &droneCover) const {
    // Maximal and minimal values of coordinates of drone cover initialized with NaN values,
    // to assign the appropriate values to them at the first pass of the data download loop.
    double coverXMin = 0.0 / 0.0, coverXMax = 0.0 / 0.0;
    double coverYMin = 0.0 / 0.0, coverYMax = 0.0 / 0.0;
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    auto result = Shape::getMinMaxValues(droneCover);
    
    return (result.xMin <= xMax && result.xMax >= xMin) && (result.yMin <= yMax && result.yMax >= yMin)
        && (result.zMin <= zMax && result.zMax >= zMin);
}
