#pragma once

#include "shape.hh"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

/**
 * @brief Models the concept of the water surface.
 */
class waterSurface : public Shape
{
    /**
     * @brief File containing the water coordinates model.
     */
    const std::string kWaterModel_ = solidDirectoryPath / "waterSurfaceModel.dat";

    /**
     * @brief Set of water coordinates.
     */
    std::vector<Vector3D> water_;

  public:
    /**
     * @brief Result file for saving the water surface coordinates.
     */
    const std::string kWaterSurfaceFile_ = solidDirectoryPath / "result" / "waterSurface.dat";

    /**
     * @brief Variable containing water surface level.
     */
    const double waterLevel_ = 200;

    /**
     * @brief Constructor for waterSurface class.
     *
     * Construct new waterSurface object.
     */
    waterSurface();

    /**
     * @brief Destructor for waterSurface class.
     *
     * Reduces the number of current Vector3D objects by the number of coordinates
     * contained in set of water coordinates.
     */
    ~waterSurface() { currentCounter_ -= water_.size(); }

    /**
     * @brief Saves the water coordinates to a result file.
     * @param filename - result file name.
     */
    void draw(const std::string& filename) const override;

    /**
     * @brief Displays error message, that in the next step drone will be above water.
     */
    void getCollisionInformation() const override
    {
        std::cerr << std::endl;
        std::cerr << "\tSTOP! The drone was stopped before sailing above water" << std::endl;
    }

    /**
     * @brief Checks if in the next step of movement drone will be above the water.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    [[nodiscard]] bool checkCollision(const std::vector<Vector3D>& droneCover) const override;
};
