#pragma once

#include "cuboid.hh"
#include "rotationMatrix.hh"

/**
 * @brief Models the concept of the drone.
 */
class Drone : public Cuboid {
    /**
     * @brief Set of left front rotor coordinates.
     */
    std::vector<Vector3D> leftFrontRotor_;

    /**
     * @brief Default translation of left front rotor.
     */
    const Vector3D leftFrontRotorTranslation_{25, 35, 20};

    /**
     * @brief Set of left rear rotor coordinates.
     */
    std::vector<Vector3D> leftRearRotor_;

    /**
     * @brief Default translation of left rear rotor.
     */
    const Vector3D leftRearRotorTranslation_{-35, 12, 20};

    /**
     * @brief Set of right front rotor coordinates.
     */
    std::vector<Vector3D> rightFrontRotor_;

    /**
     * @brief Default translation of right front rotor.
     */
    const Vector3D rightFrontRotorTranslation_{25, -35, 20};

    /**
     * @brief Set of right rear rotor coordinates.
     */
    std::vector<Vector3D> rightRearRotor_;

    /**
     * @brief Default translation of right rear rotor.
     */
    const Vector3D rightRearRotorTranslation_{-35, -12, 20};

    /**
     * @brief File containing the rotor coordinates model.
     */
    const std::string kRotorModel_ = solidDirectoryPath / "rotorModel.dat";

    /**
     * @brief Calculate translation vector.
     * @param angleChange - rise or fall angle,
     * @param distance - the distance the drone should move.
     * @return Vector3D - translation vector.
     */
    [[nodiscard]] Vector3D calculateTranslation(double angleChange, double distance) const;

    /**
     * @brief Rotates the rotors
     *
     * Function used in animation of drone movement.
     * @param frames - number of frames in animation.
     */
    void rotateRotor(const int &frames);

  public:
    /**
     * @brief Result file for saving the left front rotor coordinates.
     */
    const std::string kLeftFrontRotorFile_ = solidDirectoryPath / "result" / "leftFrontRotor.dat";

    /**
     * @brief Result file for saving the right front rotor coordinates.
     */
    const std::string kRightFrontRotorFile_ = solidDirectoryPath / "result" / "rightFrontRotor.dat";

    /**
     * @brief Result file for saving the left rear rotor coordinates.
     */
    const std::string kLeftRearRotorFile_ = solidDirectoryPath / "result" / "leftRearRotor.dat";

    /**
     * @brief Result file for saving the right rear rotor coordinates.
     */
    const std::string kRightRearRotorFile_ = solidDirectoryPath / "result" / "rightRearRotor.dat";

    /**
     * @brief Result file for saving the drone body coordinates.
     */
    const std::string kDroneFile = solidDirectoryPath / "result" / "droneBody.dat";

    /**
     * @brief Move drone forward
     * @param angleChange - rise or fall angle,
     * @param distance - the distance the drone should move,
     * @param frames - number of frames in animation.
     * @return Drone cover coordinates in the next step.
     */
    std::vector<Vector3D> moveForward(const double &angleChange, const double &distance, const int &frames);

    /**
     * @brief Rotate the drone in OZ axis.
     * @param rotateAngle - rotation angle,
     * @param frames - number of frames in animation.
     */
    void rotate(const double &rotateAngle, const int &frames);

    /**
     * @brief Constructor for Drone class.
     *
     * Construct new Drone object.
     */
    Drone();

    /**
     * @brief Destructor for Drone class.
     *
     * Reduces the number of current Vector3D objects by the number of coordinates
     * contained in sets of drone and rotors coordinates.
     */
    ~Drone() {
        currentCounter_ -=
            (leftRearRotor_.size() + leftFrontRotor_.size() + rightRearRotor_.size() + rightFrontRotor_.size());
    }

    /**
     * @brief Saves the bottom coordinates to a result file.
     * @param cuboidFilename - drone body file name,
     * @param leftFrontRotorFilename - left front rotor file name,
     * @param rightFrontRotorFilename - right front rotor file name,
     * @param leftRearRotorFilename - left rear rotor file name,
     * @param rightRearRotorFilename - right rear rotor file name.
     */
    void draw(const std::string &cuboidFilename,
              const std::string &leftFrontRotorFilename,
              const std::string &rightFrontRotorFilename,
              const std::string &leftRearRotorFilename,
              const std::string &rightRearRotorFilename) const;

    /**
     * @brief Displays error message, that in the next step will be collision with another drone.
     */
    void getCollisionInformation() const override {
        std::cerr << std::endl;
        std::cerr << "\tSTOP! The drone was stopped before collision with another drone" << std::endl;
    }

    /**
     * @brief Checks if in the next step of movement will be collision with another drone.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    [[nodiscard]] bool checkCollision(const std::vector<Vector3D> &droneCover) const override;
};

