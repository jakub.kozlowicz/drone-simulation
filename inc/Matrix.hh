#pragma once

#include <cmath>
#include <iostream>

#include "Vector.hh"

const double PI = 3.14159265;

using std::abs;

/**
 * @brief Szablon dla klasy Matrix
 *
 * Klasa Matrix przechowuje dane o wspolczynnikach przy zmiennych
 * Metody w niej zawarte sa potrzebnymi metodami do wykonywania operacji na tej macierzy,
 * oraz takimi ktore wylicza rozwiazania ukladu rownan
 *
 * @tparam T - typ przechowywanego pola
 * @tparam SIZE - rozmiar macierzy
 */
template<typename T, int SIZE>
class Matrix {
  private:
    T matrix[SIZE][SIZE]{};

    /*!
     * Funkcja do zamiany wierszy w tablicy
     * Eliminuje ona zera jesli sa na przekatnej macierzy
     * @param matrix1 - referencja do tablicy na ktorej ma byc wykonana zmiana
     * @return - ilosc zamian wierszy
     */
    int SwitchRows(Matrix<T, SIZE> &matrix1) const;

    /*!
     * Funkcja wykonujaca eliminacje Gaussa
     * Modyfikuje macierz tak aby ponizej przkatnej byly same zera
     * (tworzy tak zwana macierz trojkatna)
     * Wykorzystywana do latwego policzenia wyznacznika macierzy
     * @param matrix1 - referencja do tablicy na ktorej mam byc wykonana operacja
     */
    void GaussElimination(Matrix<T, SIZE> &matrix1) const;

  public:
    /*!
     * Operator funkcyjny dla klasy Matrix
     * pozowala na zapisywanie w tablicy w odpowiednich miejscach wartosci
     * @param row - numer wiersza
     * @param column - numer kolumny
     * @return - rzadane przez nas miejsce z tablicy
     */
    T &operator()(unsigned int row, unsigned int column);

    /*!
     * Operator funkcyjny dla klasy Matrix
     * nie pozwala na zmiane wartosci w tablicy, umozliwa tylko pobranie odpowiedniej komorki
     * @param row - numer wiersza
     * @param column - numer kolumny
     * @return - rzadane pzrez nas miejsce w tablicy
     */
    const T &operator()(unsigned int row, unsigned int column) const;

    /*!
     * Funkcja liczaca wyznacznik przy pomocy metody Gaussa
     * Przekazywana jest do niej kopia macierzy, a na tej kopii wykonywana operacje
     * Najpierw usuwane sa zera z przekatnej macierzy
     * Potem tworzona jest macierz trojkatna
     * A wyznacznik to bedzie iloczyn liczb na przekatniej macierzy
     * @param tmp - kopia tablicy
     * @return - wartosc wyznacznika
     */
    T Determinant(Matrix<T, SIZE> tmp) const;

    /*!
     * Funkcja wykonujaca transpozycje macierzy
     * Modyfikuje istniejącą macierz
     */
    void Transpose();

    /*!
     * Przeciazenie operatora mnozenia
     * pierwszym niejawnym argumentem jest macierz
     * @param vector - wektor przez ktory bedziemy mnozyc
     * @return - wektor wynikowy tego mnozenia
     */
    Vector<T, SIZE> operator*(const Vector<T, SIZE> &vector) const;

    /*!
     * Konstruktor klasy Matrix
     */
    Matrix();
};

/*!
 * Przeciazenie operatora przsunieecia bitowego do wczytywania macierzy
 * @param strm - strumien wejsciowy
 * @param matrix - tablica w ktorej zostana zapisane wartosci
 * @return - strumien wejsciowy
 */
template<typename T, int SIZE>
std::istream &operator>>(std::istream &strm, Matrix<T, SIZE> &matrix);

/*!
 * Przeciazenie operatora przsunieecia bitowego do wypisywania macierzy
 * @param strm - strumien wyjsciowy
 * @param matrix - tablica z ktorej bedziemy wypisywac wartosci
 * @return - strumien wyjsciowy
 */
template<typename T, int SIZE>
std::ostream &operator<<(std::ostream &strm, const Matrix<T, SIZE> &matrix);

template<typename T, int SIZE>
T &Matrix<T, SIZE>::operator()(unsigned int row, unsigned int column) {
    return const_cast<T &>(const_cast<const Matrix<T, SIZE> *>(this)->operator()(row, column));
}

template<typename T, int SIZE>
const T &Matrix<T, SIZE>::operator()(unsigned int row, unsigned int column) const {
    if (row >= SIZE || column >= SIZE) {
        std::cout << "Array index out of bound, exiting";
        exit(0);
    }

    return matrix[row][column];
}

template<typename T, int SIZE>
int Matrix<T, SIZE>::SwitchRows(Matrix<T, SIZE> &matrix1) const {
    int swapCounter = 0;

    for (int i = 0; i < SIZE; i++) {
        for (int k = i + 1; k < SIZE; k++) {
            if (abs(matrix1(k, i) - matrix1(i, i)) > ERR) {  //jesli wartosc elementu wyzej jest
                // mniejsza od elementu pod nim
                swapCounter++;  //zwieksz o jeden licznik zamian wierszy
                for (int j = 0; j < SIZE; j++) {
                    std::swap(matrix1(i, j), matrix1(k, j));  //zamien dwa wiersze miejscami
                }
            }
        }
    }

    return swapCounter;
}

/*
 * sprawdzenie "abs(matrix1(k, i) - matrix1(i, i)) > ERR" nie tylko wyelminuje zera na przekatnej macierzy,
 * a ustawi rowniez wiersze z mniejszymi wartosciami ponizej co ulatwi pozniej liczenie pomocniczej zmiennej w eliminacji Gaussa
 */

template<typename T, int SIZE>
void Matrix<T, SIZE>::GaussElimination(Matrix<T, SIZE> &matrix1) const {
    for (int i = 0; i < SIZE - 1; i++) {
        for (int k = i + 1; k < SIZE; k++) {
            T tmp = matrix1(k, i) / matrix1(i, i);  //pomocnicza zmienna do wyzerowania wiersza ponizej
            for (int j = 0; j < SIZE; j++) {
                matrix1(k, j) = matrix1(k, j) - (matrix1(i, j) * tmp);  //algorytm zerujacy caly wiersz
            }
        }
    }
}

template<typename T, int SIZE>
T Matrix<T, SIZE>::Determinant(Matrix<T, SIZE> tmp) const {
    T determinant;
    int switchCounter = 0;

    switchCounter = SwitchRows(tmp);

    GaussElimination(tmp);

    for (int i = 0; i < SIZE; i++) {
        if (i == 0) {
            determinant = tmp(i, i);
        } else {
            determinant = determinant * tmp(i, i);  //przemnozenie wartosci na przekatnej macierzy
        }
    }

    if (switchCounter % 2 == 0) {    //jesli liczba zamian wierszy jest parzysta
        determinant = determinant;   // wyznacznik pozostaje bez zmian
    } else {                         //jesli liczba zamian wierszy jest nieparzysta
        determinant = -determinant;  //trzeba zmienic znak wyznacznika
    }

    return determinant;
}

template<typename T, int SIZE>
void Matrix<T, SIZE>::Transpose() {
    for (int i = 0; i < SIZE; i++) {
        for (int j = i + 1; j < SIZE; j++) {
            std::swap(matrix[i][j], matrix[j][i]);
        }
    }
}

template<typename T, int SIZE>
Vector<T, SIZE> Matrix<T, SIZE>::operator*(const Vector<T, SIZE> &vector) const {
    Vector<T, SIZE> result;

    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            result[i] += matrix[i][j] * vector[j];
        }
    }

    return result;
}

template<typename T, int SIZE>
Matrix<T, SIZE>::Matrix() {
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            matrix[i][j] = 0;
        }
    }
}

template<typename T, int SIZE>
std::istream &operator>>(std::istream &strm, Matrix<T, SIZE> &matrix) {
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            strm >> matrix(i, j);
        }
    }

    return strm;
}

template<typename T, int SIZE>
std::ostream &operator<<(std::ostream &strm, const Matrix<T, SIZE> &matrix) {
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            strm << matrix(i, j) << " ";
        }
        strm << "\n";
    }

    return strm;
}
