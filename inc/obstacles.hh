#pragma once

#include "shape.hh"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

/**
 * @brief Models the concept of the rod obstacle.
 */
class Rod : public Shape
{
    /**
     * @brief Minimal and maximal values of coordinates on X axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double xMin = 0.0 / 0.0, xMax = 0.0 / 0.0;

    /**
     * @brief Minimal and maximal values of coordinates on Y axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double yMin = 0.0 / 0.0, yMax = 0.0 / 0.0;

    /**
     * @brief Minimal and maximal values of coordinates on Z axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double zMin = 0.0 / 0.0, zMax = 0.0 / 0.0;

    /**
     * @brief Set of rod obstacle coordinates.
     */
    std::vector<Vector3D> obstacleRod_;

    /**
     * @brief Default translation of rod obstacle.
     */
    const Vector3D obstacleRodTranslation_{-80, 50, 100};

    /**
     * @brief File containing the rod coordinates model.
     */
    const std::string kObstacleRodModel = solidDirectoryPath / "obstacleRodModel.dat";

  public:
    /**
     * @brief Result file for saving the rod obstacle coordinates.
     */
    const std::string kObstacleRodFile = solidDirectoryPath / "result" / "obstacleRod.dat";

    /**
     * @brief Constructor for Rod class.
     *
     * Construct new Rod object.
     */
    Rod();

    /**
     * @brief Destructor for Rod class.
     *
     * Reduces the number of current Vector3D objects by the number of coordinates
     * contained in set of rod obstacle coordinates.
     */
    ~Rod() { currentCounter_ -= obstacleRod_.size(); }

    /**
     * @brief Saves the rod obstacle coordinates to a result file.
     * @param rodFilename - result file name.
     */
    void draw(const std::string& rodFilename) const override;

    /**
     * @brief Displays error message, that in the next step will be collision with rod obstacle.
     */
    void getCollisionInformation() const override
    {
        std::cerr << std::endl;
        std::cerr << "\tSTOP! The drone was stopped before collision with the rod" << std::endl;
    }

    /**
     * @brief Checks if in the next step of movement will be collision with the rod.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    [[nodiscard]] bool checkCollision(const std::vector<Vector3D>& droneCover) const override;
};

/**
 * @brief Models the concept of the rectangle obstacle.
 */
class Rectangle : public Shape
{
    /**
     * @brief Minimal and maximal values of coordinates on X axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double xMin = 0.0 / 0.0, xMax = 0.0 / 0.0;

    /**
     * @brief Minimal and maximal values of coordinates on Y axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double yMin = 0.0 / 0.0, yMax = 0.0 / 0.0;

    /**
     * @brief Minimal and maximal values of coordinates on Z axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double zMin = 0.0 / 0.0, zMax = 0.0 / 0.0;

    /**
     * @brief Set of rectangle obstacle coordinates.
     */
    std::vector<Vector3D> obstacleRectangle_;

    /**
     * @brief Default translation of rectangle obstacle.
     */
    const Vector3D obstacleRectangleTranslation_{70, -100, -200};

    /**
     * @brief File containing the rectangle coordinates model.
     */
    std::string kObstacleRectangleModel = solidDirectoryPath / "obstacleRectangleModel.dat";

  public:
    /**
     * @brief Result file for saving the rectangle obstacle coordinates.
     */
    std::string kObstacleRectangleFile = solidDirectoryPath / "result" / "obstacleRectangle.dat";

    /**
     * @brief Constructor for Rectangle class.
     *
     * Construct new Rectangle object.
     */
    Rectangle();

    /**
     * @brief Destructor for Rectangle class.
     *
     * Reduces the number of current Vector3D objects by the number of coordinates
     * contained in set of rectangle obstacle coordinates.
     */
    ~Rectangle() { currentCounter_ -= obstacleRectangle_.size(); }

    /**
     * @brief Saves the rectangle obstacle coordinates to a result file.
     * @param rectangleFilename - result file name.
     */
    void draw(const std::string& rectangleFilename) const override;

    /**
     * @brief Displays error message, that in the next step will be collision with rectangle obstacle.
     */
    void getCollisionInformation() const override
    {
        std::cerr << std::endl;
        std::cerr << "\tSTOP! The drone was stopped before collision with the rectangle" << std::endl;
    }

    /**
     * @brief Checks if in the next step of movement will be collision with the rectangle.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    [[nodiscard]] bool checkCollision(const std::vector<Vector3D>& droneCover) const override;
};

/**
 * @brief Models the concept of the cuboid obstacle.
 */
class CuboidObstacle : public Shape
{
    /**
     * @brief Minimal and maximal values of coordinates on X axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double xMin = 0.0 / 0.0, xMax = 0.0 / 0.0;

    /**
     * @brief Minimal and maximal values of coordinates on Y axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double yMin = 0.0 / 0.0, yMax = 0.0 / 0.0;

    /**
     * @brief Minimal and maximal values of coordinates on Z axis
     *
     * Initialized with NaN values to assign the appropriate values to them at the first pass of the data download loop
     */
    double zMin = 0.0 / 0.0, zMax = 0.0 / 0.0;

    /**
     * @brief Set of cuboid obstacle coordinates.
     */
    std::vector<Vector3D> obstacleCuboid_;

    /**
     * @brief Default translation of cuboid obstacle.
     */
    const Vector3D obstacleCuboidTranslation_{100, 150, -200};

    /**
     * @brief File containing the cuboid obstacle coordinates model.
     */
    std::string kObstacleCuboidModel = solidDirectoryPath / "obstacleCuboidModel.dat";

  public:
    /**
     * @brief Result file for saving the cuboid obstacle coordinates.
     */
    std::string kObstacleCuboidFile = solidDirectoryPath / "result" / "obstacleCuboid.dat";

    /**
     * @brief Constructor for CuboidObstacle class.
     *
     * Construct new CuboidObstacle object.
     */
    CuboidObstacle();

    /**
     * @brief Destructor for CuboidObstacles class.
     *
     * Reduces the number of current Vector3D objects by the number of coordinates
     * contained in set of cuboid obstacle coordinates.
     */
    ~CuboidObstacle() { currentCounter_ -= obstacleCuboid_.size(); }

    /**
     * @brief Saves the rectangle obstacle coordinates to a result file.
     * @param cuboidObstacleFilename - result file name.
     */
    void draw(const std::string& cuboidObstacleFilename) const override;

    /**
     * @brief Displays error message, that in the next step will be collision with cuboid obstacle.
     */
    void getCollisionInformation() const override
    {
        std::cerr << std::endl;
        std::cerr << "\tSTOP! The drone was stopped before collision with the cuboid" << std::endl;
    }

    /**
     * @brief Checks if in the next step of movement will be collision with the cuboid.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    [[nodiscard]] bool checkCollision(const std::vector<Vector3D>& droneCover) const override;
};
