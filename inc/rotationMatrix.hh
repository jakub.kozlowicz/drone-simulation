#pragma once

#include "Matrix.hh"
#include "Vector.hh"

/**
 * @brief Models the concept of the rotation matrix of geometric object.
 */
class RotationMatrix {
    /**
     * @brief Variable containing the rotation matrix.
     */
    Matrix<double, 3> rotationMatrix_;

  public:
    /**
     * @brief Constructor for RotationMatrix class.
     *
     * Initialize matrix with values need in rotation on different axis.
     * @param axis - which axis,
     * @param angle - value of rotation angle.
     */
    RotationMatrix(const std::string &axis, const double &angle);

    /**
     * @brief Matrix multiplication by a vector.
     *
     * Overloading of the multiplication operator for multiplying the rotation matrix by a coordinate vector.
     * @param vector - coordinate vector.
     * @return result - result of multiplying.
     */
    Vector3D operator*(const Vector3D &vector) const;
};


