#pragma once

#include <iostream>

constexpr double ERR = 0.0000000001;

/*!
 * Klasa wektor przechowuje informacje o wektorze wyrazow wolnych,
 * mozna ja rowniez wykorzystac do przechowywania informacji o wektorze bledu czy rozwiazaniach ukladu rownan
 * Zawiera potrzebne metody do operowania na dzialaniach na wektorach
 */
template <typename T, int SIZE>
class Vector {
   private:
    T vector[SIZE]{};

   public:
    /*!
     * Przeciazenie operatora indeksowania dla klasy wektor
     * pozwala zapisywac dane w wektorze
     * @param index - numer pola w ktore chcemy zapisac dana wartosc
     * @return - pole w ktorym zapiszemy dana wartosc
     */
    T &operator[](unsigned int index);

    /*!
     * Przeciazenie operatora indeksowania dla klasy wektor
     * nie pozwala na zmiane danych w polu wektor
     * @param index - numer pola w danym wektorze
     * @return - dane pole z wektora
     */
    const T &operator[](unsigned int index) const;

    /*!
     * Przeciazenie operatora mnozenia dla mnozenia wektora przez liczbe
     * pierwszy argument jest niejawny i wskazuje na wektor z klasy
     * @param number - liczba przez ktora mnozymy
     * @return - wynik mnozenia
     */
    Vector<T, SIZE> operator*(T number) const;

    /*!
     * Przeciazenie operatora mnozenia dla iloczynu skalarnego dwoch wektorow
     * pierwszy argument jest niejawny i wskazuje na wektor z klasy
     * @param vector1 - wektor przez ktory mnozymy
     * @return - iloczyn skalarny dwoch wektorow
     */
    T operator*(const Vector<T, SIZE> &vector1) const;

    /*!
     * Przeciazenie operatora dzielenia dla dzielenia wektora przez liczbe
     * pierwszy argument jest niejawny i wskazuje na wektor z klasy
     * @param number - liczba przez ktora chcemy podzielic
     * @return - wynik dzielenia
     */
    Vector<T, SIZE> operator/(T number) const;

    /*!
     * Przeciazenie operatora dodawania dla dodawania dwoch wektorow
     * pierwszy argument jest niejawny i wskazuje na wektor z klasy
     * @param vector1 - wektor ktory chcemy dodac
     * @return - wynik dodawania dwoch wektorow
     */
    Vector<T, SIZE> operator+(const Vector<T, SIZE> &vector1) const;

    /*!
     * Przeciazenie operatora odejmowania dla roznicy dwoch wektorow
     * pierwszy argument jest niejawny i wskazuje na wektor z klasy
     * @param vector1 - wektor ktory chcemy odjac
     * @return - wynik odejmowania dwoch wektorow
     */
    Vector<T, SIZE> operator-(const Vector<T, SIZE> &vector1) const;

    /*!
     * Konstruktor dla klasy wektor
     */
    Vector();

    Vector(double arg1, double arg2, double arg3);
};

using Vector3D = Vector<double, 3>;

/*!
 * Przeciazenie operatora przesuniecia bitowego w prawo
 * dla wczytywania danych do wektora
 * @param strm - strumien wejsciowy
 * @param vector - wektor w ktorym zapiszemy nasze dane
 * @return - strumien wejsciwy
 */
template <typename T, int SIZE>
std::istream &operator>>(std::istream &strm, Vector<T, SIZE> &vector);

/*!
 * Przeciazenie operatora przesuniecia bitowego w lewo
 * dla wypisywania danych z naszego wektora
 * @param strm - strumien wyjsciowy
 * @param vector - wektor z ktorego chcemy wypisac dane
 * @return - strumien wyjsciowy
 */
template <typename T, int SIZE>
std::ostream &operator<<(std::ostream &strm, const Vector<T, SIZE> &vector);

template <typename T, int SIZE>
T &Vector<T, SIZE>::operator[](unsigned int index) {
    return const_cast<T &>(const_cast<const Vector<T, SIZE> *>(this)->operator[](index));
}

template <typename T, int SIZE>
const T &Vector<T, SIZE>::operator[](unsigned int index) const {
    if (index >= SIZE) {
        std::cout << "Array index out of bound, exiting";
        exit(0);
    }

    return vector[index];
}

template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator*(T number) const {
    Vector<T, SIZE> result;

    for (int i = 0; i < SIZE; ++i) {
        result[i] = vector[i] * number;
    }

    return result;
}

template <typename T, int SIZE>
T Vector<T, SIZE>::operator*(const Vector<T, SIZE> &vector1) const {
    T result;

    for (int i = 0; i < SIZE; ++i) {
        result += vector[i] * vector1[i];
    }

    return result;
}

template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator/(T number) const {
    Vector<T, SIZE> result;

    for (int i = 0; i < SIZE; ++i) {
        result[i] = vector[i] / number;
    }

    return result;
}

template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator+(const Vector<T, SIZE> &vector1) const {
    Vector<T, SIZE> result;

    for (int i = 0; i < SIZE; ++i) {
        result[i] = vector[i] + vector1[i];
    }

    return result;
}

template <typename T, int SIZE>
Vector<T, SIZE> Vector<T, SIZE>::operator-(const Vector<T, SIZE> &vector1) const {
    Vector<T, SIZE> result;

    for (int i = 0; i < SIZE; ++i) {
        result[i] = vector[i] - vector1[i];
    }

    return result;
}

template <typename T, int SIZE>
Vector<T, SIZE>::Vector() {
    for (int i = 0; i < SIZE; ++i) {
        vector[i] = 0;
    }
}

template <typename T, int SIZE>
Vector<T, SIZE>::Vector(double arg1, double arg2, double arg3) {
    vector[0] = arg1;
    vector[1] = arg2;
    vector[2] = arg3;
}

template <typename T, int SIZE>
std::istream &operator>>(std::istream &strm, Vector<T, SIZE> &vector) {
    for (int i = 0; i < SIZE; ++i) {
        strm >> vector[i];
    }

    return strm;
}

template <typename T, int SIZE>
std::ostream &operator<<(std::ostream &strm, const Vector<T, SIZE> &vector) {
    for (int i = 0; i < SIZE; ++i) {
        strm << vector[i];
        strm << " ";
    }

    return strm;
}
