#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <memory>

#include "gnuplot_link.hh"
#include "bottomSurface.hh"
#include "drone.hh"
#include "waterSurface.hh"
#include "obstacles.hh"

/**
 * @brief Models the concept of the stage.
 */
class Scene {
    /**
     * @brief Variable to connect to gnuplot.
     */
    PzG::GnuplotLink link_;

    /**
     * @brief Variable containing the drone.
     */
    Drone drone_;

    /**
     * @brief Variable containing the bottom surface.
     */
    bottomSurface bottom_surface_;

    /**
     * @brief Variable containing the water surface.
     */
    waterSurface water_surface_;

    /**
     * @brief Variable containing the rod obstacle.
     */
    Rod rod_;

    /**
     * @brief Variable containing the rectangle obstacle.
     */
    Rectangle rectangle_;

    /**
     * @brief Variable containing the cuboid obstacle.
     */
    CuboidObstacle cuboid_obstacle_;

    /**
     * @brief Number of frames in the animation.
     */
    const int frames_ = 120;

    /**
     * @brief Time between individual frames of animation.
     */
    const int frameFreeze_ = 16000;

    /**
     * @brief Initializes the vector container with pointers to individual elements of the scene.
     * @tparam TDrone - Drone class,
     * @tparam TGround - bottomSurface class,
     * @tparam TWater - waterSurface class,
     * @tparam TRod - Rod class,
     * @tparam TRectangle - Rectangle class,
     * @tparam TCuboid - CuboidObstacle class.
     * @return vector of pointers on individual elements of the scene.
     */
    template<typename TDrone, typename TGround, typename TWater, typename TRod, typename TRectangle, typename TCuboid>
    [[nodiscard]] std::vector<std::shared_ptr<Shape>> objectsOnScene() const;

    /**
     * @brief Check drone collision with objects on scene.
     *
     * Check drone collision with ground, water and obstacles.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    bool checkCollision(const std::vector<Vector3D> &droneCover);

  public:
    /**
     * @brief Constructor for Scene class.
     *
     * Construct new Scene object.
     * Initialize link with gnuplot and add files which will be drawn.
     */
    Scene();

    /**
     * @brief Destructor for Scene class.
     */
    ~Scene() = default;

    /**
     * @brief Implements rotation animation.
     *
     * Overloading of the animation function for the rotation of the drone in the OZ axis.
     * @param rotateAngle - rotation angle.
     */
    void animation(double rotateAngle);

    /**
     * @brief Implements movement animation.
     *
     * Overloading of the animation function for the movement of the drone.
     * @param angleChange - rise or fall angle,
     * @param distance - the distance the drone should move.
     */
    void animation(const double &angleChange, const double &distance);

    /**
     * @brief Displays the number of Vector3D objects
     *
     * Displays the total and current number of Vector3D objects
     */
    static void displayObjectCount();
};

template<typename TDrone, typename TGround, typename TWater, typename TRod, typename TRectangle, typename TCuboid>
std::vector<std::shared_ptr<Shape>> Scene::objectsOnScene() const {
    std::vector<std::shared_ptr<Shape>> objects;

    objects.push_back(std::shared_ptr<Shape>(new TDrone));
    objects.push_back(std::shared_ptr<Shape>(new TGround));
    objects.push_back(std::shared_ptr<Shape>(new TWater));
    objects.push_back(std::shared_ptr<Shape>(new TRod));
    objects.push_back(std::shared_ptr<Shape>(new TRectangle));
    objects.push_back(std::shared_ptr<Shape>(new TCuboid));

    return objects;
}