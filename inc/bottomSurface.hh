#pragma once

#include "shape.hh"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

/**
 * @brief Models the concept of the ground surface.
 */
class bottomSurface : public Shape
{
    /**
     * @brief File containing the bottom coordinates model.
     */
    const std::string kBottomModel_ = solidDirectoryPath / "bottomSurfaceModel.dat";

    /**
     * @brief Set of bottom coordinates.
     */
    std::vector<Vector3D> bottom_;

  public:
    /**
     * @brief Result file for saving the bottom surface coordinates.
     */
    const std::string kBottomSurfaceFile_ = solidDirectoryPath / "result" /"bottomSurface.dat";

    /**
     * @brief Variable containing bottom surface level.
     */
    const double bottomLevel_ = -200;

    /**
     * @brief Constructor for bottomSurface class.
     *
     * Construct new bottomSurface object.
     */
    bottomSurface();

    /**
     * @brief Destructor for bottomSurface class.
     *
     * Reduces the number of current Vector3D objects by the number of coordinates
     * contained in set of bottom coordinates.
     */
    ~bottomSurface() { currentCounter_ -= bottom_.size(); }

    /**
     * @brief Saves the bottom coordinates to a result file.
     * @param filename - result file name.
     */
    void draw(const std::string& filename) const override;

    /**
     * @brief Displays error message, that in the next step will be collision with bottom.
     */
    void getCollisionInformation() const override
    {
        std::cerr << std::endl;
        std::cerr << "\tSTOP! The drone was stopped before touching the bottom" << std::endl;
    }

    /**
     * @brief Checks if in the next step of movement will be collision with the bottom.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    [[nodiscard]] bool checkCollision(const std::vector<Vector3D>& droneCover) const override;
};
