#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "rotationMatrix.hh"
#include "shape.hh"

/**
 * @brief Models the concept of the cuboid.
 */
class Cuboid : public Shape
{
    /**
     * @brief File containing the cuboid coordinates model.
     */
    const std::string kModelCuboid_ = solidDirectoryPath / "cuboidModel.dat";

    /**
     * @brief Set of cuboid coordinates.
     */
    std::vector<Vector3D> points_;

  protected:
    /**
     * @brief Set of drone cover coordinates used in checkCollision functions.
     */
    std::vector<Vector3D> cuboidCover_;

    /**
     * @brief Translates the drone cover.
     * Moves the drone cover the same translation vector as the drone body.
     * @param change - translation vector.
     */
    void translateCover(const Vector3D& change);

    /**
     * @brief Calculate the coordinates of drone cover in next step of animation (movement)
     * @param translation - translation vector.
     * @return nextStepCover - coordinates of drone cover in next step of movement.
     */
    [[nodiscard]] std::vector<Vector3D> getNextStepCover(const Vector3D& translation) const;

  public:
    /**
     * @brief Constructor for Cuboid class.
     *
     * Construct new Cuboid object.
     */
    Cuboid();

    /**
     * @brief Destructor for Cuboid class.
     *
     * Reduces the number of current Vector3D objects by the number of coordinates
     * contained in set of cuboid coordinates.
     */
    ~Cuboid() { currentCounter_ -= points_.size(); }

    /**
     * @brief Saves the bottom coordinates to a result file.
     * @param filename - result file name.
     */
    void draw(const std::string& filename) const override;
};
