#pragma once

#include <string>
#include <vector>

#include "Matrix.hh"
#include "Vector.hh"

#include <filesystem>

const std::filesystem::path solidDirectoryPath{MODEL_DIR_PATH};

/**
 * @brief Models the concept of the geometric object.
 */
class Shape
{
  protected:
    /**
     * @brief Translation vector of geometric object.
     */
    Vector3D translation_;

    /**
     * @brief Rotation angle of geometric object.
     */
    double angle_;

    /**
     * @brief Calculate degrees to radians.
     * @param degree - value in degree.
     * @return value in radians.
     */
    static double makeRadians(double degree) { return degree * PI / 180; }

  public:
    struct minMax
    {
        double xMin, xMax;
        double yMin, yMax;
        double zMin, zMax;
    };

    /**
     * @brief Number of total Vector3D objects.
     */
    inline static unsigned allCounter_ = 0;

    /**
     * @brief Number of current Vector3D objects.
     */
    inline static unsigned currentCounter_ = 0;

    /**
     * @brief Constructor for Shape class.
     *
     * Increase the total and current number of Vector3D objects.
     */
    Shape() : angle_{0}
    {
        currentCounter_++;
        allCounter_++;
    };

    /**
     * @brief Increase translation vector.
     * @param change - shift vector.
     */
    void translate(const Vector3D& change) { translation_ = translation_ + change; }

    /**
     * @brief Increase rotation angle.
     * @param change - rotation angle.
     */
    void writeAngle(double change) { angle_ += change; }

    /**
     * @brief Saves the geometric object coordinates to a result file.
     * @param filename - result file name.
     */
    virtual void draw(const std::string& filename) const = 0;

    /**
     * @brief Displays error message, that in the next step will be collision with something.
     */
    virtual void getCollisionInformation() const = 0;

    /**
     * @brief Checks if in the next step of movement will be collision with something.
     * @param droneCover - drone cover not visible on scene.
     * @return true - in next step will be collision,
     * @return false - in next step will not be collision.
     */
    [[nodiscard]] virtual bool checkCollision(const std::vector<Vector3D>& droneCover) const = 0;

    minMax getMinMaxValues(const std::vector<Vector3D>& object) const;

    /**
     * @brief Destructor for Shape class.
     *
     * Reduces the number of current Vector3D objects.
     */
    ~Shape() { currentCounter_--; };
};
