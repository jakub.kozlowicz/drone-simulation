var class_matrix =
[
    [ "Matrix", "class_matrix.html#a3e08e51548e9fc339d66dcec03064d59", null ],
    [ "Determinant", "class_matrix.html#ac2c8303fdc06fd4c7e292455eae5c416", null ],
    [ "GaussElimination", "class_matrix.html#af127708865eb29c4157be68d2d783b65", null ],
    [ "operator()", "class_matrix.html#a8d88b4b485a4716a41159891584b0870", null ],
    [ "operator()", "class_matrix.html#a7b6c9a81b3ce322779c80af86eaa1eb4", null ],
    [ "operator*", "class_matrix.html#aa32da33cf90c74d4a8083a08a3bf1001", null ],
    [ "SwitchRows", "class_matrix.html#a4722df35d85e6bb1ac4666cd36063e52", null ],
    [ "Transpose", "class_matrix.html#ad5c9497d13380b10beaff841f7f5216f", null ],
    [ "matrix", "class_matrix.html#ab20a4e5f5647febc866fb32744f54f86", null ]
];