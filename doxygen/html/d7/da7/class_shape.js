var class_shape =
[
    [ "Shape", "d7/da7/class_shape.html#aaa8d87171e65e0d8ba3c5459978992a7", null ],
    [ "makeRadians", "d7/da7/class_shape.html#a392923e3a75a0710056ca4770af77ddc", null ],
    [ "translate", "d7/da7/class_shape.html#a499ab02e6fd00d35c87e9409cab672dc", null ],
    [ "writeAngle", "d7/da7/class_shape.html#a4ab9606f25f5121c5d09e95de802f4c0", null ],
    [ "angle", "d7/da7/class_shape.html#a1642eab9d4f9d319a3d9a38d4a69cd90", null ],
    [ "fileName", "d7/da7/class_shape.html#a555a7ac666dcb2bcd80c20371a54ce02", null ],
    [ "translation", "d7/da7/class_shape.html#a5a6c23c7dae9cfcdd93800c08968a921", null ]
];