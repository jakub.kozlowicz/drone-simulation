var files_dup =
[
    [ "bottomSurface.cpp", "bottom_surface_8cpp.html", null ],
    [ "bottomSurface.hh", "bottom_surface_8hh.html", [
      [ "bottomSurface", "classbottom_surface.html", "classbottom_surface" ]
    ] ],
    [ "cuboid.cpp", "cuboid_8cpp.html", null ],
    [ "cuboid.hh", "cuboid_8hh.html", [
      [ "Cuboid", "class_cuboid.html", "class_cuboid" ]
    ] ],
    [ "drone.cpp", "drone_8cpp.html", null ],
    [ "drone.hh", "drone_8hh.html", [
      [ "Drone", "class_drone.html", "class_drone" ]
    ] ],
    [ "gnuplot_link.cpp", "gnuplot__link_8cpp.html", "gnuplot__link_8cpp" ],
    [ "gnuplot_link.hh", "gnuplot__link_8hh.html", "gnuplot__link_8hh" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "Matrix.hh", "_matrix_8hh.html", "_matrix_8hh" ],
    [ "menu.cpp", "menu_8cpp.html", "menu_8cpp" ],
    [ "menu.hh", "menu_8hh.html", "menu_8hh" ],
    [ "obstacles.cpp", "obstacles_8cpp.html", null ],
    [ "obstacles.hh", "obstacles_8hh.html", [
      [ "Rod", "class_rod.html", "class_rod" ],
      [ "Rectangle", "class_rectangle.html", "class_rectangle" ],
      [ "CuboidObstacle", "class_cuboid_obstacle.html", "class_cuboid_obstacle" ]
    ] ],
    [ "rotationMatrix.cpp", "rotation_matrix_8cpp.html", null ],
    [ "rotationMatrix.hh", "rotation_matrix_8hh.html", [
      [ "RotationMatrix", "class_rotation_matrix.html", "class_rotation_matrix" ]
    ] ],
    [ "scene.cpp", "scene_8cpp.html", null ],
    [ "scene.hh", "scene_8hh.html", [
      [ "Scene", "class_scene.html", "class_scene" ]
    ] ],
    [ "shape.cpp", "shape_8cpp.html", null ],
    [ "shape.hh", "shape_8hh.html", [
      [ "Shape", "class_shape.html", "class_shape" ]
    ] ],
    [ "Vector.hh", "_vector_8hh.html", "_vector_8hh" ],
    [ "waterSurface.cpp", "water_surface_8cpp.html", null ],
    [ "waterSurface.hh", "water_surface_8hh.html", [
      [ "waterSurface", "classwater_surface.html", "classwater_surface" ]
    ] ]
];