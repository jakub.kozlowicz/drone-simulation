var classwater_surface =
[
    [ "waterSurface", "classwater_surface.html#a168ae86be77b2066f8981603fefaf718", null ],
    [ "~waterSurface", "classwater_surface.html#aa626eb73c254c3d94a38cab5f8978a5a", null ],
    [ "checkCollision", "classwater_surface.html#a21bd94ef24b4c75e0327c709f3bc5fac", null ],
    [ "draw", "classwater_surface.html#a0de872419d5f60025eb0b060b6df3633", null ],
    [ "getCollisionInformation", "classwater_surface.html#ab49ab5ed2dbac566bd0b30e465b2bd5b", null ],
    [ "kWaterModel_", "classwater_surface.html#a9dadccbc538edfd3738e170bfe87a834", null ],
    [ "kWaterSurfaceFile_", "classwater_surface.html#a052874e714972cf607e2aab7c91118f3", null ],
    [ "water_", "classwater_surface.html#a9ff71ccc7295259b564c3841d7fcf1bb", null ],
    [ "waterLevel_", "classwater_surface.html#a1a4eb620c37a1e16b63a74b9834b3720", null ]
];