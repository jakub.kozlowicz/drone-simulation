var gnuplot__link_8hh =
[
    [ "FileInfo", "d4/d97/class_pz_g_1_1_file_info.html", "d4/d97/class_pz_g_1_1_file_info" ],
    [ "GnuplotLink", "df/df9/class_pz_g_1_1_gnuplot_link.html", "df/df9/class_pz_g_1_1_gnuplot_link" ],
    [ "DrawingMode", "d8/d35/gnuplot__link_8hh.html#a4360c76a1dbf714a19a0d97fe56e0660", [
      [ "DM_2D", "d8/d35/gnuplot__link_8hh.html#a4360c76a1dbf714a19a0d97fe56e0660ac85b6f8146edb5ca2df8345dd86ef039", null ],
      [ "DM_3D", "d8/d35/gnuplot__link_8hh.html#a4360c76a1dbf714a19a0d97fe56e0660aa7ef207217913b87d83fdf559d8368c7", null ]
    ] ],
    [ "LineStyle", "d8/d35/gnuplot__link_8hh.html#ab0580cdb6bfe9e51d7de2588bc824076", [
      [ "LS_CONTINUOUS", "d8/d35/gnuplot__link_8hh.html#ab0580cdb6bfe9e51d7de2588bc824076af8f97c84dadf8eaa1f0370861e15dfec", null ],
      [ "LS_SCATTERED", "d8/d35/gnuplot__link_8hh.html#ab0580cdb6bfe9e51d7de2588bc824076a6495216b6a84a9fbe7141a687f9c03f1", null ]
    ] ]
];