# Underwater drone simulation
"Mini game" written in C++ for my university course called Object Oriented
Programming at Wroclaw University of Science ant Technology.
The game is about navigating the drone underwater between obstacles.

![Preview](images/preview.png)

## Usage
Program is terminal based application with graphical interface using
`gnuplot` - an interactive plotting program. All options are read from terminal
and plotted to screen.

### Options
- `r` - moving straight ahead for a given distance with a given angle of ascent
or descent,
- `o` - change the orientation of the drone (rotation in the OZ axis),
- `m` - display help menu,
- `k` - quit program.

**Value of angle is important - grater than 0 means ascent and less than 0 means
descent.**

### Collision
When there is a collision in the next movement of the drone, the program will
notify us about the error and will not allow, for the movement to take place.
At this point, we should go back or rotate the drone so as to avoid the obstacle.

This applies to movement that collides with an obstacle, as well as touching the
bottom or attempting to emerge above the water surface.

### Model files
Program use model files of objects stored in `solid` folder, read them to the
memory and do some operations on them.

*Result files are stored under* `solid/result`, *so make sure that folder exist.* 

## License
- MIT License
## Authors
- Jakub Kozłowicz - Drone movement, logic of the "game"
- MSc Piotr Dulewicz & Dr Bogdan Kreczmer - `gnuplot` API files
